package it.unipd.csc.molly;

import android.graphics.Point;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;

/**
 * Created by Luca on 08/03/2017.
 */

public class AudioWaveform implements Runnable, App.FFmpegLoadedCallback, FFmpegExecuteResponseHandler
{

    File imageFile;
    File dstFile;
    Point size;
    Callback cb;
    void generateWaveform(File imageFile, File dstFile, Point size, Callback cb){
        this.imageFile= imageFile;
        this.dstFile = dstFile;
        this.size = size;

        this.cb = cb;
        Thread worker = new Thread(this);

        worker.start();
    }

    @Override
    public void run()
    {
        App.loadFFmpeg(this);
    }

    @Override
    public void onLoaded(FFmpeg fFmpeg)
    {
        String command = "-y -i  <<input_file>> -filter_complex showwavespic=s=<<width>>x<<height>> -frames:v 1  <<output_file>>";

        command = command.replaceFirst("<<input_file>>", imageFile.getAbsolutePath());
        command = command.replaceFirst("<<width>>", this.size.x+"");
        command = command.replaceFirst("<<height>>", this.size.y+"");
        command = command.replaceFirst("<<output_file>>", dstFile.getAbsolutePath());
        Log.i(App.LOGTAG, command);

        try {
            fFmpeg.execute(command.split("\\s+"), this);
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String s)
    {
        Log.i(App.LOGTAG, s);
        cb.onSuccess(s);
    }

    @Override
    public void onProgress(String s)
    {

    }

    @Override
    public void onFailure(String s)
    {
        Log.e(App.LOGTAG, s);
    }

    @Override
    public void onStart()
    {

    }

    @Override
    public void onFinish()
    {

    }


    public static interface Callback{

        void onProgress(String message);
        void onSuccess(String message);
        void onFail(String message);

    }
}
