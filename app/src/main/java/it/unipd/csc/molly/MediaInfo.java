package it.unipd.csc.molly;

public class MediaInfo {

	public int bitrate;
	public int duration;
	public VideoInfo videoInfo;
	public AudioInfo audioInfo;

	public String toString(){
		
		String asString = "MediaInfo:\n"+
				"bitrate: "+bitrate+ "\n"+
				"duration: "+duration+ "\n";
		
		if(videoInfo != null){
			asString+=videoInfo.toString();
		}

		if(audioInfo != null){
			asString+=audioInfo.toString();
		}
		return asString;
		
	}
	
}
