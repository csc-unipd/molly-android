package it.unipd.csc.molly;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.location.DetectedActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Luca on 26/12/2016.
 */

public class AwarenessSnapshotModel
{
    public static final String LOGTAG = "molly";

    private String attributesNames [] = {"weather", "activity", "headphone", "location"};

    private Weather weather;
    private DetectedActivity activity;
    private HeadphoneState headphone;
    private Location location;


    private Map<String, Integer> attributesProcessed = new HashMap();

    public AwarenessSnapshotModel()
    {
        for(String attribute: attributesNames){
            attributesProcessed.put(attribute, 0);
        }
    }


    public Weather getWeather()
    {
        return weather;
    }


    public boolean set(String attribute, Object value)
    {
        try {

            Class fieldClass = this.getClass().getDeclaredField(attribute).getType();
            if(value == null)
            {
                this.getClass().getDeclaredField(attribute).set(this, value);
                this.attributesProcessed.put(attribute, 1);
                return true;
            }else {
                Log.i(LOGTAG, "Field class is: " + fieldClass.getCanonicalName());
                Log.i(LOGTAG, "Value class is: " + value.getClass());

                if (fieldClass.isAssignableFrom(value.getClass())) {
                    Log.i(LOGTAG, "So I set the value");
                    this.getClass().getDeclaredField(attribute).set(this, value);
                    this.attributesProcessed.put(attribute, 1);
                    return true;
                } else {
                    Log.i(LOGTAG, "So I don't set the value");
                    return false;
                }
            }

        } catch (IllegalAccessException  | NoSuchFieldException e) {
            Log.e(LOGTAG, e.getMessage());
            e.printStackTrace();
            return false;
        }

    }

    public Object get(String attribute)
    {
        try {
            return this.getClass().getDeclaredField(attribute).get(this);

        } catch (IllegalAccessException  | NoSuchFieldException e) {
            Log.e(LOGTAG, e.getMessage());
            e.printStackTrace();
            return null;
        }
    }


    public String [] attributes(){
        return attributesNames;
    }


    public boolean isProcessed()
    {
        boolean itIs = true;

        for(String attribute: attributesNames)
        {
            boolean isAttributeProcessed = attributesProcessed.get(attribute) != 0;
            Log.d(LOGTAG, "attribute \'"+attribute+"\' is "+(!isAttributeProcessed?"not":"")+" processed");
            itIs = itIs && attributesProcessed.get(attribute) != 0;
        }
        Log.d(LOGTAG, "So model is "+(!itIs?"not":"")+" processed");
        return itIs;
    }


    public JSONObject asJSON()
    {
        JSONObject thisAsJson = new JSONObject();

        Weather weather = (Weather) this.get("weather");
        Location location = (Location) this.get("location");
        HeadphoneState headphone = (HeadphoneState) this.get("headphone");
        DetectedActivity activity = (DetectedActivity) this.get("activity");

        try {
            if(weather != null){
                thisAsJson.put("weather_temperature", weather.getTemperature(Weather.CELSIUS))
                        .put("weather_conditions", new JSONArray(weather.getConditions()))
                        .put("weather_humidity", weather.getHumidity());
            }
            if(location != null){
                thisAsJson.put("location_latitude", location.getLatitude())
                        .put("location_longitude", location.getLongitude())
                        .put("location_altitude", location.getAltitude())
                        .put("location_accuracy", location.getAccuracy())
                        .put("location_time", location.getTime())
                        .put("speed", location.getSpeed());
            }

            if(activity != null){
                thisAsJson.put("activity_type", activity.getType());
                thisAsJson.put("activity_confidence", activity.getConfidence());
            }

            if(headphone != null){
                thisAsJson.put("headphone_plugged", headphone.getState() == HeadphoneState.PLUGGED_IN?1:0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return thisAsJson;
    }

}
