package it.unipd.csc.molly;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.DetectedActivityResult;
import com.google.android.gms.awareness.snapshot.HeadphoneStateResult;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Luca on 26/12/2016.
 */

public class AwarenessSnapshotController
{

    public static final String LOGTAG = "molly";
    GoogleApiClient client = null;

    static final String requiredPermissions[] = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.INTERNET
    };
    private Activity activity;

    public void acquireSnapshot(@NonNull Activity activity,
                                @NonNull final AwarenessSnapshotListener snapshotListener)
    {
        this.activity = activity;
        this.connectToGoogleApi(new Runnable()
        {
            @Override
            public void run()
            {
                loadAwarenessSnapshot(snapshotListener);
            }
        });

    }

    /**
     * ensure we have the necessary permission to use the Awareness API
     */
    private String [] checkPermission()
    {
        ArrayList<String> permisssionToAsk = new ArrayList<>();

        for (String p : requiredPermissions) {
            if (ContextCompat.checkSelfPermission(activity.getApplicationContext(), p) != PackageManager.PERMISSION_GRANTED) {
                permisssionToAsk.add(p);
            }
        }

        return permisssionToAsk.toArray(new String[permisssionToAsk.size()]);
    }

    private void connectToGoogleApi(@NonNull final Runnable toDoWhenConnected)
    {
        Log.w(LOGTAG, "connecting to google play services");

        client = new GoogleApiClient.Builder(activity.getApplicationContext())
                .addApi(Awareness.API)
                .build();

        client.registerConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener()
        {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
            {
                Log.w(LOGTAG, "connection failed");
            }
        });

        client.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks()
        {
            @Override
            public void onConnected(@Nullable Bundle bundle)
            {
                Log.w(LOGTAG, "connected");
                toDoWhenConnected.run();
            }

            @Override
            public void onConnectionSuspended(int i)
            {
                Log.w(LOGTAG, "Connection suspended");
            }
        });

        client.connect();
    }

    private void loadAwarenessSnapshot(final AwarenessSnapshotListener toDoWhenSnapshotAcquired)
    {
        String missingPermissions [] = checkPermission();
        if(missingPermissions.length != 0){
            toDoWhenSnapshotAcquired.onMissingPermissions(missingPermissions);
            return;
        }

        final AwarenessSnapshotModel snapshot = new AwarenessSnapshotModel();

        Log.i(LOGTAG, "requesting activity");
        Awareness.SnapshotApi.getDetectedActivity(client)
                .setResultCallback(new ResultCallback<DetectedActivityResult>()
                {
                    @Override
                    public void onResult(@NonNull DetectedActivityResult detectedActivityResult)
                    {
                        Status status = detectedActivityResult.getStatus();
                        DetectedActivity activity = new DetectedActivity(DetectedActivity.UNKNOWN, 0);

                        if (!status.isSuccess()) {
                            Log.w(LOGTAG, "Couldn't get activity data");
                            Log.w(LOGTAG, "Status code: " + status.getStatusCode());
                            Log.w(LOGTAG, "Status message: " + status.getStatusMessage());

                            if (status.getStatusCode() == CommonStatusCodes.TIMEOUT) {
                                Log.w(LOGTAG, "Activity request is timed out");
                            }

                        } else {
                            ActivityRecognitionResult ar = detectedActivityResult.getActivityRecognitionResult();
                            activity = ar.getMostProbableActivity();
                            Log.i(LOGTAG, "the most probable activity is:");
                            Log.i(LOGTAG, activity.toString());

                        }
                        snapshot.set("activity", activity);
                        launchAcquiredCallbackIfProcessed(snapshot, toDoWhenSnapshotAcquired);
                    }
                }, 10, TimeUnit.SECONDS);


        Log.i(LOGTAG, "requesting weather");

        Awareness.SnapshotApi.getWeather(client).setResultCallback(new ResultCallback<WeatherResult>()
        {
            @Override
            public void onResult(@NonNull WeatherResult weatherResult)
            {
                Log.i(LOGTAG, "weather request ended in: ");
                Weather weather = null;

                if (!weatherResult.getStatus().isSuccess()) {
                    Log.e(LOGTAG, "cannot get weather. +" +
                            "Status code" + weatherResult.getStatus().getStatusCode() +
                            "Status message" + weatherResult.getStatus().getStatusMessage()
                    );

                } else {
                    Log.i(LOGTAG, "result: " + weatherResult.getWeather());
                    weather = weatherResult.getWeather();
                    launchAcquiredCallbackIfProcessed(snapshot, toDoWhenSnapshotAcquired);
                }
                snapshot.set("weather", weather);
                launchAcquiredCallbackIfProcessed(snapshot, toDoWhenSnapshotAcquired);
            }
        }, 10, TimeUnit.SECONDS);

        Log.i(LOGTAG, "Requesting location");
        Awareness.SnapshotApi.getLocation(client).setResultCallback(new ResultCallback<LocationResult>()
        {
            @Override
            public void onResult(@NonNull LocationResult locationResult)
            {
                Log.i(LOGTAG, "location request ended");
                Status status = locationResult.getStatus();
                Location detectedLocation = null;
                if (!status.isSuccess()) {
                    Log.w(LOGTAG, "Couldn't get location data");
                    Log.w(LOGTAG, "Status code: " + status.getStatusCode());
                    Log.w(LOGTAG, "Status message: " + status.getStatusMessage());
                } else {
                    detectedLocation = locationResult.getLocation();
                }

                snapshot.set("location", detectedLocation);
                launchAcquiredCallbackIfProcessed(snapshot, toDoWhenSnapshotAcquired);
            }

        }, 10, TimeUnit.SECONDS);


        Log.i(LOGTAG, "Requesting headphones status");

        Awareness.SnapshotApi.getHeadphoneState(client).setResultCallback(new ResultCallback<HeadphoneStateResult>()
        {
            @Override
            public void onResult(@NonNull HeadphoneStateResult headphoneStateResult)
            {
                Log.i(LOGTAG, "location request ended");
                Status status = headphoneStateResult.getStatus();
                HeadphoneState detectedLocation = null;
                if (!status.isSuccess()) {
                    Log.w(LOGTAG, "Couldn't get headphones data");
                    Log.w(LOGTAG, "Status code: " + status.getStatusCode());
                    Log.w(LOGTAG, "Status message: " + status.getStatusMessage());
                } else {
                    detectedLocation = headphoneStateResult.getHeadphoneState();
                }

                snapshot.set("headphone", detectedLocation);
                launchAcquiredCallbackIfProcessed(snapshot, toDoWhenSnapshotAcquired);
            }
        }, 10, TimeUnit.SECONDS);
    }

    private void launchAcquiredCallbackIfProcessed(AwarenessSnapshotModel model,
                                                   AwarenessSnapshotListener toDoWhenSnapshotAcquired)
    {
        if (model.isProcessed()) {
            toDoWhenSnapshotAcquired.onSnapshotAcquired(model);
        }
    }

    public interface AwarenessSnapshotListener
    {
        void onSnapshotAcquired(AwarenessSnapshotModel model);

        void onMissingPermissions(String permissions[]);
    }
}
