package it.unipd.csc.molly;

public class AudioInfo
{

	public int bitrate;
	public int sampleRate;
	public String codec;
	
	
public String toString(){
		
		
		return "AudioInfo:\n"+
			"bitrate: "+bitrate+ "\n"+
			"sampleRate: "+sampleRate+ "\n"+
			"codec: "+codec+ "\n";
		
	}
}
