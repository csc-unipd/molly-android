package it.unipd.csc.molly;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONException;

import java.util.ArrayList;

public class WebViewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener
{

    private final static int FILECHOOSER_RESULTCODE = 1;
    static final int PERMISSION_REQUEST_CODE = 222;
    private ValueCallback<Uri[]> userChooseFilesCallback;
    WebView contentView;
    SwipeRefreshLayout swipeLayout;

    String webAppDomain;
    String webAppEntryPoint;

    JavascriptAppInterface javascriptInterface;

    PendingSnapshot pendingSnapshot = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web_view);

        webAppDomain = getString(R.string.web_app_domain);
        webAppEntryPoint = "http://" + webAppDomain + "/";

        contentView = (WebView) findViewById(R.id.content_web_view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            contentView.setWebContentsDebuggingEnabled(true);
        }

        contentView.setWebViewClient(new CustomWebViewClient());

        contentView.setWebChromeClient(new MollyAppWebChromeClient());
        contentView.getSettings().setJavaScriptEnabled(true);
        javascriptInterface = new JavascriptAppInterface();
        contentView.addJavascriptInterface(javascriptInterface, "Molly");

       // contentView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
       // contentView.getSettings().setAppCacheEnabled(false);
       // contentView.clearCache(true);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeLayout.setOnRefreshListener(this);

        contentView.loadUrl(webAppEntryPoint);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent)
    {
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (resultCode != RESULT_OK) {
                Log.w(App.LOGTAG, "Not ok result from filechooser");
                userChooseFilesCallback.onReceiveValue(null);
                userChooseFilesCallback = null;
                return;
            }

            if (userChooseFilesCallback == null) return;

            Log.i(App.LOGTAG, "User had choose his files");
            Log.i(App.LOGTAG, intent.getData().getPath());
            userChooseFilesCallback.onReceiveValue(new Uri[]{intent.getData()});
        }
    }

    @Override
    public void onRefresh()
    {
        contentView.reload();
    }

    /**
     * Interfaccia java-javascript inserita nella webview
     */
    private class JavascriptAppInterface
    {

        /**
         * funzione invocabile nel codice JavaScript
         *
         * @param jsFunctionToCallWithData funzione javascript da chiamare una volta acquisito lo snapshot
         */
        @JavascriptInterface
        public void beginAwarenessSnapshotCapturing(final String jsFunctionToCallWithData)
        {
            Log.i(App.LOGTAG, "Start snapshot acquiring");
            AwarenessSnapshotController snapshotController = new AwarenessSnapshotController();

            snapshotController.acquireSnapshot(WebViewActivity.this, new AwarenessSnapshotController.AwarenessSnapshotListener()
            {

                @Override
                public void onSnapshotAcquired(AwarenessSnapshotModel model)
                {
                    Log.i(App.LOGTAG, "Snapshot acquired:");
                    Log.i(App.LOGTAG, model.toString());

                    final ArrayList<String> listp = new ArrayList<>();
                    for (String s : model.attributes()) {
                        Object attribute = model.get(s);
                        if (attribute != null) {
                            Log.i(App.LOGTAG, "Model has attribute '" + s + "' value: " + attribute.toString());
                            listp.add(attribute.toString());
                        }
                    }

                    String jsCommand = "javascript:" + jsFunctionToCallWithData + "('" + model.asJSON() + "')";
                    Log.i(App.LOGTAG, "Invoking js: " + jsCommand);
                    contentView.loadUrl(jsCommand);

                    try {
                        Log.i(App.LOGTAG, model.asJSON().toString(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onMissingPermissions(String[] permissions)
                {
                    pendingSnapshot = new PendingSnapshot();
                    pendingSnapshot.javascriptCallback = jsFunctionToCallWithData;
                    ActivityCompat.requestPermissions(WebViewActivity.this,
                            permissions,
                            PERMISSION_REQUEST_CODE);
                }
            });
        }
    }

    /**
     * Questa classe permette di controllare il caricamento
     * delle pagine
     */
    private class CustomWebViewClient extends WebViewClient
    {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            swipeLayout.setRefreshing(true);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            Log.i(App.LOGTAG, "Finished loading URL: " +url);
            Log.i(App.LOGTAG, view.getOriginalUrl()+" "+view.getUrl());
            swipeLayout.setRefreshing(false);
            super.onPageFinished(view, url);
        }

        /**
         * Questo metodo nonostante sia deprecato è l'unico risultato
         * funzionante
         *
         * Reindirizza le richieste al di fuori del dominio della web app
         * verso l'app nel sistema appropriata, es: mappe
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view,
                                                String url)
        {
            Log.i(App.LOGTAG, "requesting url: " + url);

            if (url.contains(webAppDomain)) {
                view.loadUrl(url);
                return true;
            } else {
                Log.i(App.LOGTAG, "Opening with other app");
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
                return false;
            }
        }

    }

    private class MollyAppWebChromeClient extends WebChromeClient
    {
        public boolean onShowFileChooser(
                WebView webView, ValueCallback<Uri[]> userChooseFilesCallback,
                WebChromeClient.FileChooserParams fileChooserParams)
        {
            Log.i(App.LOGTAG, "User needs a file");

            WebViewActivity.this.userChooseFilesCallback = userChooseFilesCallback;

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String[] mimeTypes = fileChooserParams.getAcceptTypes()[0].split(",");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

            Intent i = new Intent(WebViewActivity.this, FileChooserActivity.class);
            startActivityForResult(i, WebViewActivity.FILECHOOSER_RESULTCODE);

            return true;
        }
    }

    //
    // definisce cosa succee alla pressione di un tasto
    //
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                //se viene premuto back faccio un passo indietro nella cronologia
                //della webview
                case KeyEvent.KEYCODE_BACK:
                    if (contentView.canGoBack()) {
                        contentView.goBack();
                    } else {
                        //se sono giunto alla prima pagina visitata esco
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if(pendingSnapshot != null){
                        javascriptInterface.beginAwarenessSnapshotCapturing(pendingSnapshot.javascriptCallback);
                        pendingSnapshot = null;
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    class PendingSnapshot{
        String  javascriptCallback;
    }

}
