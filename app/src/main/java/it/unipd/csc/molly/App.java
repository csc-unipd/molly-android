package it.unipd.csc.molly;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by Luca on 27/12/2016.
 */

public class App
{

    public static final String LOGTAG = "molly";
    public static Context context;

    public static void toast(String msg){

        (Toast.makeText(context, msg, Toast.LENGTH_SHORT)).show();
    }

    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else
            if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }



    public static String prettyTime(int millis){

        int seconds = millis/1000;
        int minutes = seconds / 60;
        seconds = seconds % 60;
        Log.w(App.LOGTAG,millis+" "+seconds+" "+minutes);
        String sSeconds = ""+(seconds>=10?seconds:"0"+seconds);
        String sMinutes = ""+(minutes>=10?minutes:"0"+minutes);

        return sMinutes+":"+sSeconds;
    }

    public static String prettySize(int bytes)
    {
        DecimalFormat df = new DecimalFormat("#.0");
        if(bytes<1024*1024){
                //< 1MB
                return (bytes/1024) + "KB";
            }else if(bytes < 1024 * 1024*1024){
                float mb = ((float)bytes/1024 / 1024);
                return  df.format(mb) + "MB";
            }else{
                float gb = ((float)bytes/1024 / 1024 /1024);
                return  df.format(gb) + "GB";
        }
    }


    static FFmpeg ffmpeg = null;
    public static void loadFFmpeg(final FFmpegLoadedCallback cb)
    {

        if (ffmpeg != null) {
            cb.onLoaded(ffmpeg);
        }

        ffmpeg = FFmpeg.getInstance(context);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler()
            {

                @Override
                public void onStart()
                {
                    Log.i(App.LOGTAG, "FFmpeg load start");
                }

                @Override
                public void onFailure()
                {
                    Log.e(App.LOGTAG, "FFmpeg load fail");
                    ffmpeg = null;
                }


                @Override
                public void onSuccess()
                {

                    cb.onLoaded(ffmpeg);
                }

                @Override
                public void onFinish()
                {
                    Log.i(App.LOGTAG, "FFmpeg load start");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
            return;
        }
    }

    public static interface FFmpegLoadedCallback
    {
        public void onLoaded(FFmpeg fFmpeg);
    }


    public static File getTempDir(){
        String tp = context.getFilesDir().getAbsolutePath() + "/temp";
        File td = new File(tp);
        if(!td.exists()){
            td.mkdir();
        }

        return td;
    }
}
