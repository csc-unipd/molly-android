package it.unipd.csc.molly;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.text.TextUtils;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by Luca on 05/03/2017.
 */
public class MediaProcessor
{

    private Context context;
    Config config;
    Callback cb = null;

    public MediaProcessor(Context c){
        this.context = c;
    }

    public void process(Config config)
    {
        MyThread worker = new MyThread( config);

        worker.setPriority(Thread.MAX_PRIORITY);
        worker.start();
    }

    void setCallback(Callback cb){
        this.cb = cb;
    }

    private class MyThread extends Thread{
        Config config;


        MyThread(Config config){
            this.config = config;
        }

        @Override
        public void run()
        {
            final Callback cb = MediaProcessor.this.cb;
            if(!loadConfig()){
                Log.e(App.LOGTAG, "Invalid VideoProcessorConfiguration");
                return;
            }
            final FFmpeg ffmpeg = FFmpeg.getInstance(context);
            try {
                    ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                    @Override
                    public void onStart() {
                        Log.i(App.LOGTAG, "FFmpeg load start");
                    }

                    @Override
                    public void onFailure() {
                        Log.i(App.LOGTAG, "FFmpeg load fail");

                        if(cb != null){
                            cb.onFail("Cannot load ffmpeg");
                        }
                    }

                    @Override
                    public void onSuccess() {

                        if(cb != null){
                            cb.onProgress("Start processing");
                        }

                        Log.i(App.LOGTAG, "FFmpeg loaded successfully");
                        Log.i(App.LOGTAG, "File to process: "+ config.srcPath);
                        String command [] = buildFFmpegCommand();

                        Log.i(App.LOGTAG, TextUtils.join(" ", command));

                        try {
                            ffmpeg.execute(command, new ExecuteBinaryResponseHandler(){
                                @Override
                                public void onStart() {
                                    Log.i(App.LOGTAG, "FFmpeg process start");
                                }

                                @Override
                                public void onProgress(String message) {

                                    Log.i(App.LOGTAG, "FFmpeg progress: "+message);
                                    String tag = "frame=";

                                    if(message.startsWith(tag)){
                                        //its a real progress
                                        String frame = message.substring(tag.length(), message.indexOf("fps"));
                                        frame=frame.trim();
                                        Log.i(App.LOGTAG, "Frame: "+frame);
                                        int iFrame = Integer.parseInt(frame);
                                        Log.i(App.LOGTAG, "Frame: "+iFrame);
                                        float fps = config.policy.info.videoInfo.fps;
                                        float percentage  = 0.1f * (float) iFrame/( config.policy.getTargetDuration()*fps);
                                        if(percentage > 100){
                                            percentage = 100;
                                        }
                                        if(cb != null) {
                                            cb.onProgress((int) percentage + "%");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(String message) {
                                    String msg  = "FFmpeg process fail: "+message;
                                    Log.e(App.LOGTAG,msg );

                                    if(cb != null){
                                        cb.onFail(msg);
                                    }
                                }

                                @Override
                                public void onSuccess(String message) {
                                    Log.i(App.LOGTAG, "FFmpeg process success: "+message);

                                    File f = new File(config.dstPath);
                                    Log.w(App.LOGTAG, "FINISHED: "+ f.getAbsolutePath());
                                    Log.w(App.LOGTAG, f.exists()? "esiste": "non esiste");

                                    MediaScannerConnection.scanFile(MediaProcessor.this.context, new String[] {config.dstPath}, null, null);

                                    String msg  = "FFmpeg process success: "+message;
                                    Log.i(App.LOGTAG,msg );

                                    if(cb != null){
                                        cb.onSuccess("complete");
                                    }
                                }

                                @Override
                                public void onFinish() {}
                            });
                        } catch (FFmpegCommandAlreadyRunningException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFinish() {
                        Log.i(App.LOGTAG, "FFmpeg load start");
                    }
                });
            } catch (FFmpegNotSupportedException e) {
                // Handle if FFmpeg is not supported by device
            }
        }

        private String[] buildFFmpegCommand(){

            String command = "-y ";
            ProcessingPolicy p = config.policy;
            if(p.cut){
                command += buildCutStartParam()+" ";
            }
            command += " -i "+this.config.srcPath+" ";

            if(p.cut){
                command += buildCutEndParam()+" ";
            }

            if(p.hasVideo()){
                command +=" -map v:0:0 ";
                if(p.resizeVideo){
                    String scaleParam =  "scale="+1280+":"+720;
                    command +="-vf "+scaleParam+" ";
                }

                if(p.changeVideoBitrate){
                    String param =  "-b:v "+p.targetVideoBitrate +"k -maxrate "+p.targetVideoMaxBitrate +"k ";
                    command +=param;
                }

                //command += " -preset ultrafast -strict -2 ";
                if(p.resizeVideo || p.changeVideoBitrate){
                    command += "-c:v:0 libx264 ";
                }else{
                    command += "-c:v:0 copy ";
                }
            }
            if(p.hasAudio()){
                command += " -map a:0:0 ";

                if(p.changeAudioBitrate || p.changeAudioSampleRate){
                    command += " -c:a:0 libmp3lame ";
                    command += " -b:a:0 128k ";
                }else{
                    command += " -c:a:0 copy ";
                }

                if(p.changeAudioSampleRate){
                    command += " -ar "+p.targetAudioSampleRate+" ";
                }
            }

            command += this.config.dstPath;
            return command.split("\\s+");
        }


        private boolean loadConfig(){
            ProcessingPolicy p = config.policy;
            if(p.cut && (p.cutEnd <= p.cutStart || p.cutStart < 0)){
                return false;
            }

            if (!(new File(config.srcPath)).isFile()) {

                return false;
            }
            return true;

        }

        private String buildCutStartParam(){

            return " -ss "+formatMillisToSec(this.config.policy.cutStart, "0.000");
        }

        private String buildCutEndParam(){

            return " -t "+formatMillisToSec(this.config.policy.cutEnd - this.config.policy.cutStart, "0.000");
        }

        private String formatMillisToSec(int millis, String format){
            DecimalFormat f = new DecimalFormat(format);
            return f.format((float)millis/1000);
        }
    }


    static public  class Config{
        public String srcPath;
        public String dstPath;
        public ProcessingPolicy policy;
    }


    static interface Callback{

        void onProgress(String message);
        void onSuccess(String message);
        void onFail(String message);

    }
}
