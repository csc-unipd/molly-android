package it.unipd.csc.molly;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.identity.intents.AddressConstants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileChooserActivity extends AppCompatActivity
{

    private static final int MEDIA_PROCESSING_REQUEST_CODE = 223;
    final int FILE_REQUEST_CODE = 1234;
    final int STORAGE_PERMISSION_REQUEST_CODE = 1235;
    String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_chooser);

        App.context = this.getApplicationContext();
        Log.i(App.LOGTAG, "Checking permissions");
        ensurePermissionGranted();

        //empty temp files created before
        //it is an easy way
        File tempDir = App.getTempDir();

        if (tempDir.isDirectory()) {
            String[] children = tempDir.list();
            for (int i = 0; i < children.length; i++) {
                Log.i(App.LOGTAG, "deleting" + children[i]);
                new File(tempDir, children[i]).delete();
            }
        }
        openFileIntent();
    }

    private void ensurePermissionGranted()
    {

        int pw = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int pr = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE);


        if (pr != pw || pw != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST_CODE);
        }
    }


    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults)
    {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                boolean granted = true;
                if (grantResults.length > 0) {
                    for (int r : grantResults) {
                        if (r != PackageManager.PERMISSION_GRANTED) {
                            granted = false;
                        }
                    }

                    if (granted) {
                        return;
                    }
                }

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                this.finish();
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void openFileChooser()
    {
        Log.i(App.LOGTAG, "Opening file chooser");
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode,
//                                    Intent intent)
//    {
//
//
//        Uri contentUri;
//        switch (requestCode) {
//            case FILE_REQUEST_CODE:
//
//                if (resultCode != RESULT_OK) {
//                    Log.i(App.LOGTAG, "User is undecided");
//                    setResult(resultCode);
//                    finish();
//                } else {
//
//                    contentUri = intent.getData();
//                    ContentResolver cR = getApplicationContext().getContentResolver();
//                    String type = cR.getType(contentUri);
//
//                    Log.i(App.LOGTAG, "Content type: " + type);
//                    App.toast("Choosen a " + type);
//
//
//                    launchContentArrangement(contentUri);
//                }
//
//
//                // filePathField.setText(filePath);
//                //  startBtn.setEnabled(true);
//                break;
//
//            case MEDIA_PROCESSING_REQUEST_CODE:
//
//
//                if (resultCode != RESULT_OK) {
//                    //we don't have a file
//
//                    //so stay here and pick another one
//
//                    //setResult(resultCode);
//                    openFileChooser();
//                    return;
//
//                } else {
//                    Intent returnIntent = new Intent();
//                    contentUri = intent.getData();
//                    Log.w(App.LOGTAG, "File Processed: " + contentUri.toString());
//
//                    returnIntent.setData(contentUri);
//                    setResult(Activity.RESULT_OK, returnIntent);
//                }
//
//                finish();
//            default:
//                break;
//        }
//        super.onActivityResult(requestCode, resultCode, intent);
//    }

    private void launchContentArrangement(Uri contentUri, String type)
    {
        filePath = App.getPath(this.getApplicationContext(), contentUri);
        Log.i(App.LOGTAG, filePath);

        //FFmpeg doesn't work well with path with spaces inside
        //so we create a temporary link with no spaces to the file
        try {
            filePath = createLinkToFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
            this.finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
            this.finish();
        }

        if (type == null) {
            ContentResolver cR = getApplicationContext().getContentResolver();
            type = cR.getType(contentUri);
        }

        if (type.startsWith("video")) {
            Intent i = new Intent(FileChooserActivity.this, VideoCutActivity.class);
            i.putExtra("filePath", filePath);

            startActivityForResult(i, MEDIA_PROCESSING_REQUEST_CODE);
        } else if (type.startsWith("image")) {
            processImage(filePath);
        } else if (type.startsWith("audio")) {
            Intent i = new Intent(FileChooserActivity.this, AudioCutActivity.class);
            i.putExtra("filePath", filePath);

            startActivityForResult(i, MEDIA_PROCESSING_REQUEST_CODE);
        }

    }

    private void processImage(String filePath)
    {

        ImageProcessor ip = new ImageProcessor();

        try {
            final File dstFile = File.createTempFile("tmp", ".jpg", App.getTempDir());
            ip.process(new File(filePath), dstFile, new ImageProcessor.Callback()
            {
                @Override
                public void onProgress(String message)
                {

                }

                @Override
                public void onSuccess(String message)
                {
                    Intent returnIntent = new Intent();
                    Uri contentUri = Uri.fromFile(dstFile);
                    Log.w(App.LOGTAG, "File Processed: " + contentUri.toString());

                    returnIntent.setData(contentUri);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }

                @Override
                public void onFail(String message)
                {

                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private Uri outputImageFile;
    private Uri outputVideoFile;
    File imagefile = null;
    File videoFile = null;
    private void openFileIntent()
    {

        // Determine Uri of camera image to save.

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "molly_" + timeStamp + ".jpg";
        String videoFileName = "molly_" + timeStamp + ".mp4";
        imagefile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+File.separator+imageFileName);
        videoFile =  new File(getExternalFilesDir(Environment.DIRECTORY_MOVIES)+File.separator+videoFileName);

        Log.i(App.LOGTAG, imagefile.exists()?"esiste":" non esiste");
        Log.i(App.LOGTAG, videoFile.exists()?"esiste":" non esiste");


        outputImageFile = Uri.fromFile(imagefile);
        outputVideoFile = Uri.fromFile(videoFile);

        // Camera.
        final List<Intent> otherIntents = new ArrayList<Intent>();
        final Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(imageCaptureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(imageCaptureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageFile);
            otherIntents.add(intent);
        }

        final Intent videoCaptureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        final List<ResolveInfo> listVideo = packageManager.queryIntentActivities(videoCaptureIntent, 0);
        for (ResolveInfo res : listVideo) {
            int icon = res.getIconResource();
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(videoCaptureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputVideoFile);
            otherIntents.add(intent);
           // otherIntents.add(new LabeledIntent(intent, res.activityInfo.packageName, "Video", icon));
        }


        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("*/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, otherIntents.toArray(new Parcelable[otherIntents.size()]));

        startActivityForResult(chooserIntent, FILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        Uri contentUri;

        if (requestCode == FILE_REQUEST_CODE) {

            if (resultCode != RESULT_OK) {
                Log.i(App.LOGTAG, "User is undecided");
                setResult(resultCode);
                finish();
            } else {

                boolean isImage = false;
                boolean isVideo = false;


                isImage = imagefile.exists();
                isVideo = videoFile.exists();

                String type = null;
                if (isImage) {
                    type = "image";
                    contentUri = outputImageFile;
                } else if (isVideo) {
                    type = "video";
                    contentUri = outputVideoFile;
                } else {
                    contentUri = intent == null ? null : intent.getData();
                }

                Log.i(App.LOGTAG, contentUri.getPath());

                if (type == null) {
                    ContentResolver cR = getApplicationContext().getContentResolver();
                    type = cR.getType(contentUri);
                }
                Log.i(App.LOGTAG, "Content type: " + type);
                App.toast("Choosen a " + type);

                launchContentArrangement(contentUri, type);
            }
        } else if (requestCode == MEDIA_PROCESSING_REQUEST_CODE) {

            if (resultCode != RESULT_OK) {
                //we don't have a file

                //so stay here and pick another one

                //setResult(resultCode);
                openFileIntent();
                return;

            } else {
                Intent returnIntent = new Intent();
                contentUri = intent.getData();
                Log.w(App.LOGTAG, "File Processed: " + contentUri.toString());

                returnIntent.setData(contentUri);
                setResult(Activity.RESULT_OK, returnIntent);
            }

            finish();
        }

        super.onActivityResult(requestCode, resultCode, intent);

    }

    String createLinkToFile(String filePath) throws IOException, InterruptedException
    {

        String filename = new File(filePath).getName();
        Log.i(App.LOGTAG, filename);
        String extension = filename.substring(filename.lastIndexOf(".") + 1);
        Log.i(App.LOGTAG, extension);
        String linkPath = App.getTempDir() + "/temp." + extension;

        Process p = Runtime.getRuntime().exec(new String[]{
                "ln",
                "-fs",
                filePath,
                linkPath,
        });

        p.waitFor();

        Log.i(App.LOGTAG, "link exit value: " + p.exitValue());

        return linkPath;
    }


}
