package it.unipd.csc.molly;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class CreateAudioContentActivity extends AppCompatActivity
{

    static final String LOGTAG = "molly";


    ListView contextSignalList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_audio_content);

        contextSignalList = (ListView) findViewById(R.id.context_signals_list);

        Thread thread = Thread.currentThread();
        Thread.UncaughtExceptionHandler wrapped = thread.getUncaughtExceptionHandler();
        if (!(wrapped instanceof GoogleApiFixUncaughtExceptionHandler)) {
            GoogleApiFixUncaughtExceptionHandler handler = new GoogleApiFixUncaughtExceptionHandler(wrapped);
            thread.setUncaughtExceptionHandler(handler);
        }

        getAwarenessSnapshot();
    }


    private class GoogleApiFixUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler
    {

        private final Thread.UncaughtExceptionHandler mWrappedHandler;

        public GoogleApiFixUncaughtExceptionHandler(Thread.UncaughtExceptionHandler wrappedHandler)
        {
            mWrappedHandler = wrappedHandler;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e)
        {

            if (e instanceof SecurityException &&
                    e.getMessage().contains("Invalid API Key for package")) {
                e.printStackTrace();
                Log.w("molly", "google error");

//                CreateAudioContentActivity.this.connectGoogleApi();
                return;
            }


            // resend the exception
            mWrappedHandler.uncaughtException(t, e);
        }
    }


    private void getAwarenessSnapshot()
    {
        AwarenessSnapshotController snapshotController = new AwarenessSnapshotController();

//        snapshotController.acquireSnapshot(this, new AwarenessSnapshotController.AwarenessSnapshotListener()
//        {
//
//            @Override
//            public void doWithSnapshot(AwarenessSnapshotModel model)
//            {
//                Log.i(LOGTAG, "Snapshot acquired:");
//                Log.i(LOGTAG, model.toString());
//
//
//                final ArrayList<String> listp = new ArrayList<>();
//                for(String s: model.attributes()){
//                    Object attribute = model.get(s);
//                    if(attribute != null){
//                        listp.add(attribute.toString());
//                    }
//                }
//
//                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateAudioContentActivity.this, android.R.layout.simple_list_item_1, listp);
//                contextSignalList.setAdapter(adapter);
//
//            }
//        });
    }

}
