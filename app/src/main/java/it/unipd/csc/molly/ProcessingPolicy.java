package it.unipd.csc.molly;

/**
 * Created by Luca on 07/03/2017.
 */

public class ProcessingPolicy
{

    private static final int MAX_ADMISSIBLE_VIDEO_BITRATE = 5000;
    private static final int DEFAULT_TARGET_VIDEO_BITRATE = 4000;
    private static final int MAX_VIDEO_DIMENSION = 1280;
    private static final int MAX_ADMISSIBLE_AUDIO_BITRATE = 129;
    private static final int DEFAULT_TARGET_AUDIO_BITRATE = 128;
    private static final int DEFAULT_TARGET_AUDIO_SAMPLERATE = 44100;
    private static final int MAX_ADMISSIBLE_AUDIO_SAMPLERATE = 44200;

    public boolean changeVideoBitrate = false;
    public int targetVideoBitrate = 0;
    public int targetVideoMaxBitrate;

    public boolean resizeVideo =false;
    public boolean cut = false;
    public int cutStart = 0;
    public int cutEnd = 0;

    public MediaInfo info;
    public int targetWidth;
    public int targetHeight;


    public boolean changeAudioBitrate = false;
    public boolean changeAudioSampleRate = false;
    public int targetAudioSampleRate ;
    public int targetAudioBitrate;

    public ProcessingPolicy(MediaInfo info){
        this.info = info;
        calculatePolicy();
    }

    public boolean hasVideo(){
        return info.videoInfo != null;
    }

    public boolean hasAudio(){
        return info.audioInfo != null;
    }

    private void calculatePolicy(){

        if(hasVideo()){
            if(info.videoInfo.bitrate > MAX_ADMISSIBLE_VIDEO_BITRATE){
                changeVideoBitrate = true;
                targetVideoBitrate = DEFAULT_TARGET_VIDEO_BITRATE;
                targetVideoMaxBitrate = MAX_ADMISSIBLE_VIDEO_BITRATE;

                if(isVideoLarge()){
                    resizeVideo = true;

                    int width = info.videoInfo.width;
                    int height = info.videoInfo.height;

                    if(width >= height){
                        targetWidth = MAX_VIDEO_DIMENSION;
                        targetHeight = (int)((float) targetWidth / width * height);
                    }else{
                        targetHeight = MAX_VIDEO_DIMENSION;
                        targetWidth = (int)((float) targetHeight / width * height);
                    }
                }
            }
        }

        if(hasAudio()){

            if(!info.audioInfo.codec.startsWith("mp3") || info.audioInfo.bitrate > MAX_ADMISSIBLE_AUDIO_BITRATE){
                changeAudioBitrate = true;
                targetAudioBitrate = DEFAULT_TARGET_AUDIO_BITRATE;
            }

            if(info.audioInfo.sampleRate > MAX_ADMISSIBLE_AUDIO_SAMPLERATE){
                changeAudioSampleRate = true;
                targetAudioSampleRate = DEFAULT_TARGET_AUDIO_SAMPLERATE;
            }

        }

        cut = true;
        cutStart = 0;
        cutEnd = this.info.duration;
    }


    public int estimatedSize(){
        int videoSize = 0, audioSize = 0;
        if(hasVideo()) {
            int bitrateVideo = changeVideoBitrate ? targetVideoMaxBitrate : info.videoInfo.bitrate;

            int sizePerSec = (int) ((float) bitrateVideo / 8); //in bytes/seconds
            float duration = (float) getTargetDuration() / 1000; //in seconds

            videoSize = (int) (sizePerSec * duration) * 1024;
        }

        if(hasAudio()){
            int bitrateAudio = changeAudioBitrate ? targetAudioBitrate : info.audioInfo.bitrate;

            int sizePerSec = (int) ((float) bitrateAudio / 8); //in bytes/seconds
            float duration = (float) getTargetDuration() / 1000; //in seconds

            audioSize = (int) (sizePerSec * duration) * 1024;
        }

        return videoSize + audioSize;
    }

    public int getTargetDuration(){
        if(cut){

            return cutEnd - cutStart;
        }else{
            return info.duration;
        }
    }

    private boolean isVideoLarge(){
        if(info.videoInfo != null && (info.videoInfo.width > MAX_VIDEO_DIMENSION ||info.videoInfo.height > MAX_VIDEO_DIMENSION )){
            return true;
        }

        return false;
    }


    public boolean needProcessing()
    {
        return true;
    }
}
