package it.unipd.csc.molly;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.edmodo.rangebar.RangeBar;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;


public class VideoCutActivity extends AppCompatActivity
{

    int mediaDuration = -1;

    final int FILE_REQUEST_CODE = 1234;
    final int  WRITE_PERMISSION_REQUEST_CODE = 1235;

    File selectedFile;

    RangeBar rangebar;
    MediaPlayer mp;

    int leftBarPos;
    int rightBarPos;
    int barTickCount;
    VideoView surface;
    FrameLayout videoBox;

    TextView startTime, estimatedSize, endTime, processProgress;

    FrameLayout overlay;
    Button proceed;

    ProcessingPolicy processingPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_cut);

        Intent startIntent = getIntent();

        Bundle extras = startIntent.getExtras();
        final String filePath = extras.getString("filePath");

        if(filePath == null){
            String msg = "No file received";
            Log.e(App.LOGTAG, msg);
            App.toast(msg);
        }else{
            Log.i(App.LOGTAG, filePath);
        }

        selectedFile = new File(filePath);
        if(!selectedFile.exists() || !selectedFile.isFile()){
            Log.e(App.LOGTAG, "File: "+selectedFile.getAbsolutePath()+" is not an existent file");
        }

        rangebar = (RangeBar) findViewById(R.id.rangebar);
        videoBox = (FrameLayout) findViewById(R.id.videoBox);
        rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
                update();
            }
        });

        TextView tv = (TextView) findViewById(R.id.fileName);
        tv.setText(selectedFile.getName());


        startTime = (TextView) findViewById(R.id.startTime);
        endTime = (TextView) findViewById(R.id.endTime);
        estimatedSize = (TextView) findViewById(R.id.estimatedSize);

        proceed = (Button) findViewById(R.id.proceed);

        proceed.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                VideoCutActivity.this.startProcessing();
            }
        });
        overlay = (FrameLayout) findViewById(R.id.overlay);
        overlay.setVisibility(View.INVISIBLE);

        processProgress = (TextView) findViewById(R.id.processProgress);

        MediaInfoExtractor extractor = new MediaInfoExtractor(this.selectedFile.getAbsolutePath());
        extractor.extractMediaInfo(new MediaInfoExtractor.OnInfoExtractedCallback()
        {
            @Override
            public void onInfoExtracted(MediaInfo info)
            {
                Log.i(App.LOGTAG, info.toString());
                processingPolicy = new ProcessingPolicy(info);
                update();

            }
        });
        surface = (VideoView)findViewById(R.id.surfaceView);
        surface.setVideoPath(this.selectedFile.getAbsolutePath());
        surface.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {

                VideoCutActivity.this.mp = mp;

                mp.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener()
                {
                    @Override
                    public void onSeekComplete(MediaPlayer mp)
                    {

                        Log.i(App.LOGTAG, "Media player seek");

                        if(mp.isPlaying())
                            mp.pause();
                    }
                });

                barTickCount = 1000;

                rangebar.setTickCount(barTickCount);
                rangebar.setThumbIndices(0, barTickCount-1);

                mp.seekTo(1);
                update();
            }
        });
    }

    private void startProcessing()
    {

        overlay.setVisibility(View.VISIBLE);

        MediaProcessor vp = new MediaProcessor(getApplicationContext());

        final MediaProcessor.Config cf = new MediaProcessor.Config();
        cf.srcPath = selectedFile.getAbsolutePath();
        String extension = cf.srcPath.substring(cf.srcPath.lastIndexOf("."));

        DecimalFormat f = new DecimalFormat("#.000");

        File dstFile = null;
        try {
            dstFile = File.createTempFile("tmp", extension, App.getTempDir());
            cf.dstPath = dstFile.getAbsolutePath();
            cf.policy = processingPolicy;
            vp.setCallback(new MediaProcessor.Callback(){

                @Override
                public void onProgress(String message)
                {
                    processProgress.setText(message);
                }

                @Override
                public void onSuccess(String message)
                {
                    processProgress.setText(message);

                    Log.i(App.LOGTAG, "I should exit now");

                    MediaInfoExtractor extractor = new MediaInfoExtractor(cf.dstPath);
                    extractor.extractMediaInfo(new MediaInfoExtractor.OnInfoExtractedCallback()
                    {
                        @Override
                        public void onInfoExtracted(MediaInfo info)
                        {
                            Log.i(App.LOGTAG, info.toString());
                            Intent returnIntent = new Intent();
                            returnIntent.setData(Uri.fromFile(new File(cf.dstPath)));
                            setResult(Activity.RESULT_OK,returnIntent);

                            VideoCutActivity.this.finish();
                        }
                    });

                }

                @Override
                public void onFail(String message)
                {
                    processProgress.setText(message);
                }
            });

            vp.process(cf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    private void update(){

        if(mp == null){
            return;
        }

        int seekPos = 0;
        int barPosToSeek;
        int leftThumbIndex = rangebar.getLeftIndex();
        int rightThumbIndex = rangebar.getRightIndex();

        Log.i(App.LOGTAG, leftThumbIndex+" "+rightThumbIndex);

        if(leftThumbIndex != leftBarPos){
            //changed left bar position
            leftBarPos = leftThumbIndex;
            barPosToSeek =  leftBarPos;

        }else{
            //changed right bar position
            rightBarPos = rightThumbIndex;
            barPosToSeek =  rightBarPos;
        }

        float relativeLeft = barPosToSeek / (float) barTickCount;
        seekPos =(int)(relativeLeft * mp.getDuration());
        Log.i(App.LOGTAG, "new seek pos: "+seekPos+ " d: "+mp.getDuration());

        mp.seekTo(seekPos);

        float leftTime = getLeftTime();
        float rightTime = getRightTime();

        startTime.setText(App.prettyTime((int)leftTime));
        endTime.setText(App.prettyTime((int)rightTime));

        if(processingPolicy != null){
            processingPolicy.cutStart = (int)leftTime;
            processingPolicy.cutEnd = (int)rightTime;

            int estimatedSizeBytes = processingPolicy.estimatedSize();
            Log.w(App.LOGTAG, "bytes: "+estimatedSizeBytes);
            estimatedSize.setText(App.prettySize(estimatedSizeBytes));
        }else{
            Log.w(App.LOGTAG, "processing processing null");
        }

    }


    public int getEstimatedSize()
    {
        long sizeInBytes  = selectedFile.length();
        float fraction =  (getRightTime() - getLeftTime()) / mp.getDuration();

        return (int)( fraction * sizeInBytes);
    }

    private float getLeftTime(){
        return (float) this.leftBarPos/this.barTickCount * mp.getDuration();
    }

    private float getRightTime(){
        return (float) this.rightBarPos/this.barTickCount * mp.getDuration();
    }

}
