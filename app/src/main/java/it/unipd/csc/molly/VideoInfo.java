package it.unipd.csc.molly;

public class VideoInfo {

	public int bitrate;
	public int width;
	public int height;
	public float fps;
	
	
public String toString(){
		
		
		return "VideoInfo:\n"+
			"bitrate: "+bitrate+ "\n"+
			"width: "+width+ "\n"+
			"height: "+height+ "\n"+
			"fps: "+fps+ "\n";
		
	}
}
