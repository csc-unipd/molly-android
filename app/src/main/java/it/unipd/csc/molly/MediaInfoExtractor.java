/**
 * Questa classe ricava le informazioni di cui abbiamo bisogno
 * sui file audio e video
 * <p>
 * Per ottenerle si usa un piccolo workaround: viene eseguito un comando
 * FFmpeg usando come parametro un solo file di input (quello di cui si vogliono le info)
 * Questo genera un errore FFmpeg, viene infatti chiamata la funzione failure(), perchè non
 * è specificato un file di destinazione.
 * <p>
 * Nell'output di questo comando sono presenti però informazioni riguardo il file di
 * input e i suoi stream che vengono lette nella funzione parse()
 */


package it.unipd.csc.molly;

import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MediaInfoExtractor implements App.FFmpegLoadedCallback, FFmpegExecuteResponseHandler
{

    static String command = "ffmpeg -y -i ";

    MediaInfo mediaInfo;
    String duration = "";
    String globalBitRate = "";
    String filePath;
    private OnInfoExtractedCallback cb;

    public MediaInfoExtractor(String mediaPath)
    {
        this.filePath = mediaPath;
    }

    public void extractMediaInfo(OnInfoExtractedCallback cb)
    {
        this.cb = cb;
        App.loadFFmpeg(this);
    }

    public MediaInfo parse(String output)
    {

        Log.w(App.LOGTAG, "PARSING INFOS");
        mediaInfo = new MediaInfo();
        // split output in lines
        String[] lines = output.split("\\r?\\n");

        // find out Duration line
        String durationLine = null;
        int lineIndex;
        for (lineIndex = 0; lineIndex < lines.length; ++lineIndex) {
            if (lines[lineIndex].matches("^(\\s)*Duration(.)*")) {
                durationLine = lines[lineIndex];
                break;
            }
        }
        if (durationLine == null) {
            Log.e(App.LOGTAG, "Invalid file");
            return null;
        }
        Log.w(App.LOGTAG, durationLine);

        String[] keyValueEntries = durationLine.split(",");

        for (String entry : keyValueEntries) {
            Pattern entryPattern = Pattern.compile("^(\\s)*(\\w+):(\\s*)(\\w.*)");
            Matcher m = entryPattern.matcher(entry);
            if (m.find()) {
                String key = m.group(2).toLowerCase();
                String value = m.group(4);

                Log.i(App.LOGTAG, key + ": " + value);
                switch (key) {
                    case "duration":
                        this.duration = value;


                        mediaInfo.duration = durationToMillis(duration);
                        break;
                    case "bitrate":
                        this.globalBitRate = value;

                        mediaInfo.bitrate = Integer.parseInt(value.substring(0, value.indexOf(" ")));
                        break;
                }
            }
        }

        // find out streams lines
        for (; lineIndex < lines.length; ++lineIndex) {
            if (lines[lineIndex].matches("^(\\s)*Stream(.)*")) {
                //this is a stream line
                String streamLine = lines[lineIndex];
                Pattern streamLinePattern = Pattern.compile("^(\\s)*Stream #0:(\\d).*: (\\w+): (.+)");
                Matcher m = streamLinePattern.matcher(streamLine);

                if (m.find()) {
                    String streamIndex = m.group(2).toLowerCase();
                    String streamType = m.group(3).toLowerCase();
                    String streamInfo = m.group(4);

                    Log.i(App.LOGTAG, "Stream index: " + streamIndex);
                    Log.i(App.LOGTAG, "streamType: " + streamType);


                    //remove secondary information into streamInfo line to avoid errors
                    Log.i(App.LOGTAG, "streamInfo: " + streamInfo);
                    streamInfo = streamInfo.replaceAll("\\([^\\)]*\\)", "");
                    streamInfo = streamInfo.replaceAll("\\[[^\\]]*\\]", "");
                    streamInfo = streamInfo.replaceAll("\\s*,\\s*", ",");
                    Log.i(App.LOGTAG, "streamInfo: " + streamInfo);
                    if (streamType.equals("video")) {

                        mediaInfo.videoInfo = new VideoInfo();

                        Pattern streamInfoPattern = Pattern.compile("^(\\w.+),\\s?(\\w.+),\\s?(\\d+x\\d+).*,\\s?(\\d+) kb/s.*,\\s?(\\d+(\\.\\d+)?) fps.*$");
                        Matcher sm = streamInfoPattern.matcher(streamInfo);

                        String infos[] = streamInfo.split(",");


                        String codec = infos[0];
                        String colorSpace = infos[1];
                        String resolution = infos[2];
                        String bitrate = "0";
                        String frameRate ="0";
                        for(int i = 3; i<infos.length; ++i){
                            if(infos[i].contains("kb/s")){
                                bitrate = infos[i];
                            }else if(infos[i].contains("fps")){
                                frameRate = infos[i];
                                break;
                            }
                        }


                        Log.i(App.LOGTAG, "Stream bitrate: " + bitrate);
                        Log.i(App.LOGTAG, "resolution: " + resolution);
                        Log.i(App.LOGTAG, "framerate: " + frameRate);

                        mediaInfo.videoInfo.bitrate = Integer.parseInt(bitrate.substring(0, bitrate.indexOf(" ")));

                        mediaInfo.videoInfo.width = Integer.parseInt(resolution.split("x")[0]);
                        mediaInfo.videoInfo.height = Integer.parseInt(resolution.split("x")[1]);
                        mediaInfo.videoInfo.fps = Float.parseFloat(frameRate.substring(0, frameRate.indexOf(" ")));
                    } else if (streamType.equals("audio")) {

                        AudioInfo info = new AudioInfo();

                        Pattern streamInfoPattern = Pattern.compile("^(\\w.+),(\\d+) Hz,(\\w+).*,(.+,)*(\\d+) kb/s.*$");
                        Matcher sm = streamInfoPattern.matcher(streamInfo);
                        if (sm.find()) {

                            String codec = sm.group(1).split(" ")[0];
                            String sampleRate = sm.group(2);
                            ;
                            String channelType = sm.group(3);

                            String bitrate = sm.group(5);

                            Log.i(App.LOGTAG, "Stream bitrate: " + bitrate);

                            info.sampleRate = Integer.parseInt(sampleRate);
                            info.codec = codec;
                            info.bitrate = Integer.parseInt(bitrate);

                        }
                        mediaInfo.audioInfo = info;
                    }

                    if (mediaInfo.audioInfo != null && mediaInfo.videoInfo != null) {
                        //we can stop here
                        break;
                    }

                }
            }
        }
        Log.i(App.LOGTAG, mediaInfo.toString());
        return mediaInfo;
    }


    public static int durationToMillis(String duration)
    {
        String[] units = duration.split(":");
        int millis = 0;
        float seconds = Float.parseFloat(units[units.length - 1]);
        millis += seconds * 1000;
        int minutes = Integer.parseInt(units[units.length - 2]);
        millis += minutes * 60 * 1000;
        int hours = Integer.parseInt(units[units.length - 3]);
        millis += hours * 60 * 60 * 1000;
        return millis;
    }

    @Override
    public void onLoaded(FFmpeg fFmpeg)
    {
        String command = buildCommand();
        try {
            fFmpeg.execute(command.split("\\s+"), this);
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private String buildCommand()
    {
        String command = "-y -i ";
        command = command + this.filePath;
        Log.i(App.LOGTAG, command);
        return command;
    }

    @Override
    public void onSuccess(String s)
    {
        //ffmpeg command succeed

        Log.i(App.LOGTAG, "success");
        Log.i(App.LOGTAG, s);

    }

    @Override
    public void onProgress(String s)
    {
        Log.i(App.LOGTAG, s);
    }

    @Override
    public void onFailure(String output)
    {
        //failure is good, we want ffmpeg to fail because we didn't specify an output file
        //in order to get just info about input file
        Log.i(App.LOGTAG, "failure");

        MediaInfo info = this.parse(output);
        this.cb.onInfoExtracted(info);
    }

    @Override
    public void onStart()
    {

    }

    @Override
    public void onFinish()
    {

    }

    public static interface OnInfoExtractedCallback
    {
        void onInfoExtracted(MediaInfo info);
    }
}