package it.unipd.csc.molly;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edmodo.rangebar.RangeBar;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;


public class AudioCutActivity extends AppCompatActivity implements RangeBar.OnRangeBarChangeListener
{

    ImageView imageView = null;
    private File selectedFile;
    int waveformWidth = -1 ;
    int waveformHeight = -1;
    RangeBar rangebar;
    int rangeTickCount = -1;

    View leftCut, rightCut, portion;
    TextView startTime, estimatedSize, endTime, processProgress;
    Button proceed;

    private MediaInfoExtractor mediaInfoExtractor;
    private MediaInfo mediaInfo;

    ProcessingPolicy processingPolicy;
    private FrameLayout overlay;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_cut);
        Bundle extras = getIntent().getExtras();
        final String filePath = extras.getString("filePath");

        if(filePath == null){
            String msg = "No file received";
            Log.e(App.LOGTAG, msg);
            App.toast(msg);
            finish();
        }else{
            Log.i(App.LOGTAG, filePath);

        }

        selectedFile = new File(filePath);
        imageView = (ImageView) findViewById(R.id.imageView);

        ViewTreeObserver vto = imageView.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                final int imageW = imageView.getMeasuredHeight();
                final int imageH = imageView.getMeasuredWidth();
                rangebar.setTickCount(imageW);
                rangeTickCount = imageW;
                update();
                try {
                    final File waveformFile = File.createTempFile("tmp", ".png", getFilesDir());
                    AudioWaveform waveform = new AudioWaveform();

                    waveform.generateWaveform(selectedFile, waveformFile, new Point(imageW, imageH), new AudioWaveform.Callback()
                    {
                        @Override
                        public void onProgress(String message)
                        {

                        }

                        @Override
                        public void onSuccess(String message)
                        {
                            Log.i(App.LOGTAG, message);
                            imageView.setImageURI(Uri.fromFile(waveformFile));
                            waveformWidth = imageW;
                            waveformHeight = imageH;
                            update();

                        }

                        @Override
                        public void onFail(String message)
                        {
                            Log.e(App.LOGTAG, message);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    String msg = "ERROR: CANNOT PROCESS AUDIO FILE";
                    Log.e(App.LOGTAG, msg);
                    App.toast(msg);
                }
                return true;
            }
        });


        rangebar = (RangeBar) findViewById(R.id.rangebarAudio);
        rangebar.setOnRangeBarChangeListener(this);

        leftCut = findViewById(R.id.waveformLeftCut);
        rightCut = findViewById(R.id.waveformRightCut);
        portion = findViewById(R.id.waveformPortion);

        mediaInfoExtractor = new MediaInfoExtractor(selectedFile.getAbsolutePath());
        mediaInfoExtractor.extractMediaInfo(new MediaInfoExtractor.OnInfoExtractedCallback()
        {
            @Override
            public void onInfoExtracted(MediaInfo info)
            {
                Log.i(App.LOGTAG, "We have media info");
                AudioCutActivity.this.mediaInfo = info;

                processingPolicy = new ProcessingPolicy(info);
                update();
            }
        });


        startTime = (TextView) findViewById(R.id.startTime);
        endTime = (TextView) findViewById(R.id.endTime);
        estimatedSize = (TextView) findViewById(R.id.estimatedSize);
        proceed = (Button) findViewById(R.id.proceed);

        proceed.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startProcessing();
            }
        });

        processProgress = (TextView) findViewById(R.id.processProgress);

        overlay = (FrameLayout) findViewById(R.id.overlay);
        overlay.setVisibility(View.GONE);

    }

    private void startProcessing()
    {

            overlay.setVisibility(View.VISIBLE);
            processingPolicy.cutStart = (int)getLeftTime();
            processingPolicy.cutEnd = (int)getRightTime();

            if(!processingPolicy.needProcessing()){
                Intent returnIntent = new Intent();
                returnIntent.setData(Uri.fromFile(selectedFile));
                setResult(Activity.RESULT_OK,returnIntent);

                AudioCutActivity.this.finish();
                return;
            }

            MediaProcessor vp = new MediaProcessor(getApplicationContext());

            final MediaProcessor.Config cf = new MediaProcessor.Config();
            cf.srcPath = selectedFile.getAbsolutePath();

            String extension = processingPolicy.hasVideo()?".mp4":".mp3";

            DecimalFormat f = new DecimalFormat("#.000");

            File dstFile = null;
            try {
                dstFile = File.createTempFile("tmp", extension, App.getTempDir());
                cf.dstPath = dstFile.getAbsolutePath();
                cf.policy = processingPolicy;
                vp.setCallback(new MediaProcessor.Callback(){

                    @Override
                    public void onProgress(String message)
                    {
                        processProgress.setText(message);
                    }

                    @Override
                    public void onSuccess(String message)
                    {
                        processProgress.setText(message);

                        Log.i(App.LOGTAG, "I should exit now");

                        Intent returnIntent = new Intent();
                        returnIntent.setData(Uri.fromFile(new File(cf.dstPath)));
                        setResult(Activity.RESULT_OK,returnIntent);

                        AudioCutActivity.this.finish();
                    }

                    @Override
                    public void onFail(String message)
                    {
                        processProgress.setText(message);
                    }
                });

                vp.process(cf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    @Override
    public void onIndexChangeListener(RangeBar rangeBar, int i, int i1)
    {
        update();
    }

    private void update()
    {
        if(rangeTickCount == -1){
            return;
        }
        int leftIndex = rangebar.getLeftIndex();
        int rightIndex = rangebar.getRightIndex();
        int tickCount = rangeTickCount;

        float relLeft = (float) leftIndex / tickCount;
        float relRight = (float) rightIndex / tickCount;

        LinearLayout.LayoutParams p = (LinearLayout.LayoutParams) leftCut.getLayoutParams();

        p.weight = relLeft;
        leftCut.setLayoutParams(p);

        p = (LinearLayout.LayoutParams) rightCut.getLayoutParams();

        p.weight = 1.0f - relRight;
        rightCut.setLayoutParams(p);

        p = (LinearLayout.LayoutParams) portion.getLayoutParams();

        p.weight = (relRight-relLeft);
        portion.setLayoutParams(p);

        Log.i(App.LOGTAG, relLeft+" "+relRight);

        float leftTime = getLeftTime();
        float rightTime = getRightTime();
        Log.i(App.LOGTAG, leftTime+" "+rightTime);

        startTime.setText(App.prettyTime((int)leftTime));
        endTime.setText(App.prettyTime((int)rightTime));
        if(processingPolicy != null){
            processingPolicy.cutStart = (int)leftTime;
            processingPolicy.cutEnd = (int)rightTime;

            int estimatedSizeBytes = processingPolicy.estimatedSize();
            Log.w(App.LOGTAG, "bytes: "+estimatedSizeBytes);
            estimatedSize.setText(App.prettySize(estimatedSizeBytes));
        }
    }

    private float getLeftTime(){
        if(mediaInfo == null) return -1;

        return (float) rangebar.getLeftIndex()/this.rangeTickCount * mediaInfo.duration;
    }

    private float getRightTime(){
        if(mediaInfo == null) return -1;

        return (float) rangebar.getRightIndex()/this.rangeTickCount * mediaInfo.duration;
    }
}
